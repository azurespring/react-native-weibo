import { NativeModules } from 'react-native';

export interface RNLoginEvent {
    userID: string;
    accessToken: string;
    refreshToken: string;
    expireAt: string;
}

export interface RNWeiboStatic {

    register( appId: string ): Promise<void>;
    login( redirectURI: string, scope: string[] ): Promise<RNLoginEvent>;
    debug( on: boolean ): Promise<void>;

    handleOpenURL( url: string ): void;

}

export var RNWeibo: RNWeiboStatic = NativeModules.RNWeibo;
