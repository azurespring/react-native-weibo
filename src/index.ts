import { RNWeibo } from './RNWeibo';
import { Linking } from 'react-native';
import * as moment from 'moment';

export interface LoginEvent {
    userID: string;
    accessToken: string;
    refreshToken: string;
    expireAt: moment.Moment;
}

export function register( appId: string ): Promise<void> {
    Linking.addEventListener( 'url', handleOpenURL );

    return RNWeibo.register( appId );
}

export function destroy() {
    Linking.removeEventListener( 'url', handleOpenURL );
}

export function login( redirectURI: string, scope: string[] ): Promise<LoginEvent> {
    return (
        RNWeibo.
            login( redirectURI, scope ).
            then( e => ({
                ...e,
                expireAt: moment( e.expireAt ),
            }) ));
}

export function debug( on: boolean ): Promise<void> {
    return RNWeibo.debug( on );
}

// ////////////////////////////////////////////////////////////

function handleOpenURL( event: { url: string } ): void {
    RNWeibo.handleOpenURL( event.url );
}
