//
//  RNWeibo.h
//  RNWeibo
//
//  Created by tsai phan on 02/05/2017.
//  Copyright © 2017 tsai phan. All rights reserved.
//

#import <React/RCTBridgeModule.h>

@interface RNWeibo : NSObject <RCTBridgeModule>

@end
