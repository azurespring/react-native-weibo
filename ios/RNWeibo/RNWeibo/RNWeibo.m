//
//  RNWeibo.m
//  RNWeibo
//
//  Created by tsai phan on 02/05/2017.
//  Copyright © 2017 tsai phan. All rights reserved.
//

#import "RNWeibo.h"
#import <WeiboSDK.h>

@interface RNWeibo() <WeiboSDKDelegate>

@end

@implementation RNWeibo {

    RCTPromiseResolveBlock resolveLogin;
    RCTPromiseRejectBlock rejectLogin;

}

RCT_EXPORT_MODULE();

RCT_EXPORT_METHOD(register: (NSString *) appId
                  resolver: (RCTPromiseResolveBlock) resolve
                  rejecter: (RCTPromiseRejectBlock) reject)
{
    if ([WeiboSDK registerApp: appId])
        resolve( nil );
    else
        reject( @"-1", @"failure", nil );
}

RCT_EXPORT_METHOD(debug: (BOOL) on
                  resolver: (RCTPromiseResolveBlock) resolve
                  rejecter: (RCTPromiseRejectBlock) reject)
{
    [WeiboSDK enableDebugMode: on];
    resolve( nil );
}

RCT_EXPORT_METHOD(handleOpenURL: (NSString *) url)
{
    [WeiboSDK handleOpenURL: [NSURL URLWithString: url] delegate: self];
}

RCT_EXPORT_METHOD(login: (NSString *) redirectURI
                  scope: (NSArray<NSString *> *) scope
                  resolver: (RCTPromiseResolveBlock) resolve
                  rejecter: (RCTPromiseRejectBlock) reject)
{
    if ( rejectLogin != nil )
        rejectLogin( @"-1", @"login", nil );

    resolveLogin = resolve;
    rejectLogin = reject;

    WBAuthorizeRequest *req = [WBAuthorizeRequest request];
    req.redirectURI = redirectURI;
    req.scope = [scope componentsJoinedByString: @","];

    [WeiboSDK sendRequest: req];
}

#pragma mark - WeiboSDKDelegate

- (void) didReceiveWeiboRequest: (WBBaseRequest *) request
{
}

- (void) didReceiveWeiboResponse: (WBBaseResponse *) response
{
    if ([response isKindOfClass: WBAuthorizeResponse.class]) {
        if ( resolveLogin != nil ) {
            WBAuthorizeResponse *aResp = (WBAuthorizeResponse *) response;
            if ( aResp.statusCode != WeiboSDKResponseStatusCodeSuccess )
                rejectLogin([NSString stringWithFormat: @"%ld", aResp.statusCode], @"failure", nil);
            else
                resolveLogin(@{ @"userID": aResp.userID,
                                @"accessToken": aResp.accessToken,
                                @"refreshToken": aResp.refreshToken,
                                @"expireAt": aResp.expirationDate,
                                });

            resolveLogin = nil;
            rejectLogin = nil;
        }
    }
}

@end
