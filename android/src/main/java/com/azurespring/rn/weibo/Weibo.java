package com.azurespring.rn.weibo;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.BaseActivityEventListener;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.WritableMap;

import com.sina.weibo.sdk.auth.AuthInfo;
import com.sina.weibo.sdk.auth.Oauth2AccessToken;
import com.sina.weibo.sdk.auth.WbAuthListener;
import com.sina.weibo.sdk.auth.WbConnectErrorMessage;
import com.sina.weibo.sdk.auth.sso.SsoHandler;
import com.sina.weibo.sdk.WbSdk;

import android.app.Activity;
import android.content.Intent;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Weibo extends ReactContextBaseJavaModule {

    private final BaseActivityEventListener activityEventListener =
        new BaseActivityEventListener() {

            public void onActivityResult( Activity activity, int requestCode, int resultCode, Intent intent ) {
                if ( ssoHandler != null )
                    ssoHandler.authorizeCallBack( requestCode, resultCode, intent );
            }

        };

    private String appId;
    private SsoHandler ssoHandler;

    public Weibo( ReactApplicationContext context ) {
        super( context );

        context.addActivityEventListener( activityEventListener );
    }

    @Override
    public String getName() {
        return "RNWeibo";
    }

    @ReactMethod
    public void register( String appId, final Promise promise ) {
        this.appId = appId;

        promise.resolve( null );
    }

    @ReactMethod
    public void login( String redirectURI, ReadableArray scope, final Promise promise ) {
        StringBuffer buffer = new StringBuffer();
        for ( int i = 0; i < scope.size(); i++ ) {
            if ( buffer.length() > 0 )
                buffer.append( "," );

            buffer.append( scope.getString(i) );
        }

        Activity currentActivity = getCurrentActivity();
        AuthInfo authInfo = new AuthInfo( currentActivity, this.appId, redirectURI, buffer.toString() );
        WbSdk.install( currentActivity, authInfo );

        ssoHandler = new SsoHandler( currentActivity );
        ssoHandler.authorize(new WbAuthListener() {

                public void onSuccess(final Oauth2AccessToken token) {
                    SimpleDateFormat dateFormat = new SimpleDateFormat( "yyyy-MM-dd'T'HH:mm:ss.SSSZ" );

                    WritableMap map = Arguments.createMap();
                    map.putString( "userID", token.getUid() );
                    map.putString( "accessToken", token.getToken() );
                    map.putString( "refreshToken", token.getRefreshToken() );
                    map.putString( "expireAt", dateFormat.format(new Date( token.getExpiresTime() )) );

                    promise.resolve( map );
                }

                public void onFailure( WbConnectErrorMessage e ) {
                    promise.reject( "-1", e.getErrorMessage() );
                }

                public void cancel() {
                    promise.reject( "-1", "cancel" );
                }

            });
    }

    @ReactMethod
    public void debug( boolean on, final Promise promise ) {
        promise.resolve( null );
    }

    @ReactMethod
    public void handleOpenURL( String url ) {
    }

}
