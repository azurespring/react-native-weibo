react-native-weibo
==================

A React Native native module, which wraps the WeiboSDK.

Installation
------------

	npm install --save @phanalpha/react-native-weibo
	react-native link @phanalpha/react-native-weibo

### Android

[Configure `repositories`](https://github.com/sinaweibosdk/weibo_android_sdk):

	maven {
	    url "https://dl.bintray.com/thelasterstar/maven/"
	}

### iOS

Integrate the [SDK](https://github.com/sinaweibosdk/weibo_ios_sdk), and
set [Linking](http://facebook.github.io/react-native/docs/linking.html) up.

[Define custom paths](http://help.apple.com/xcode/mac/8.3/#/deva52afe8a4):

	WEIBO_SDK_DIR=
